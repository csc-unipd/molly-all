#ISTRUZIONI#

Questa repository git è clonabile con:
git clone --recursive https://Bikappa@bitbucket.org/Bikappa/molly-all.git molly


#Web application#

Nel sistema devono essere installati i programmi ffmpeg e ffprobe a livello
globale.

*****SERVER*****
Installare il web server nginx:
sudo apt-get install nginx

Si può testare il funzionamento andando all'indirizzo localhost nel browser

All'interno del file di configurazione di Nginx "molly.conf" individuare le occorrenze:
root  /*pathtowebapp*/web;
alias  /*pathtowebapp*/files;

e modificarla affinché indichi il percorso verso la cartella "web" della cartella
"webapp"

Spostare questo file nei file di configurazione di Nginx:
sudo mv molly.conf /etc/nginx/sites-enabled/molly.conf

NB: la procedura classica per nginx e' di inserire il file di 
configurazione del sito nella cartella /etc/nginx/sites-available
e successivamente creare un link omonimo nella cartella
/etc/nginx/sites-enabled
Questo permette di disabilitare un sito semplicemente rimuovendo il
link in "sites-enabled" senza perdere la sua configurazione

testare la configurazione e riavviare nginx:
sudo nginx -t
sudo nginx -s reload

e' necessario modificare il file /etc/hosts affinche' il dominio
molly.dev punti a localhost

Inserire quindi le righe:
127.0.0.1 molly.dev
127.0.0.1 www.molly.dev


Installare php-fpm:
sudo apt-get install php7.0-fpm php7.0-common php-mbstring php-dom php-mysql

Nel file:
/etc/php/7.0/fpm/php.ini 
individuare le righe:
"cgi.fix_pathinfo" e impostare tale variabile a 0
"upload_max_filesize" e impostar a "200M"
"max_execution_time" e impostare a "0"
"max_input_time" = -1
"memory_limit" = 512M

Ricaricare la configurazione:
sudo service php7.0-fpm restart

*****DB*****
---INSTALLAZIONE DA ZERO---
Installare mysql: 
NB: l'importante è sapere la password dell'utente root di mysql
quindi prendere nota quando viene chiesto di impostarla
sudo apt-get install mysql-server mysql-common

Seguire le istruzioni visualizzate dall'installatore

configurare l'installazione di mysql lanciando
sudo mysql_secure_installation

seguire le istrizioni riportate...
Rispondere "yes" alle domande:
-remove test tables
-disallow root login remotely
-reload privileges table

---DB GIA' INSTALLATO---
Creare database:
A questo punto siamo pronti per creare il database sfruttando il file createdb.sql

mysql -p -u root < createdb.sql 

NB: nel nostro caso: mysql -p -u studenti < createdb.sql 

inserire la password dell'utente root di mysql precedentemente impostata.
Oltre al database verrà creato un utente con tutti i privilegi su di esso:
l'utente è "molly" con password "yllom". Questo utente sarà usato
dall'applicazione.

NB: nell'installazione su pc server si e' usato l'utente studenti. Una copia della configurazione molly e' stata salvata.

Il database viene creato con un utente che puo' loggarsi con le credenziali:
user: mario.rossi@studenti.unipd.it
password: fazzoletti

NB: e' un account professore, ma  al momento non cambia

---PACKET MANAGER---

Installare il package manager composer:

sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

Spostiamoci nella cartella della web application (webapp)

Installare i pacchetti necessari:
composer global require "fxp/composer-asset-plugin:^1.2.0"
composer install


---PERMESSI---
Le cartelle e i file della directory webapp devono essere accessibili da nginx
Entrare nella cartella e usare il seguente comando: 
sudo chown *utenteOwner*:www-data ./* -R

In questo caso si usa: 
sudo chown csc:www-data ./* -R

NB: www-data e' l'utente di default

Cambiare i permessi in modo che il gruppo "www-data" possa
leggere e scrivere i file:
sudo chmod 775 ./* -R

Installare node:
sudo apt install npm
npm install -g less
ln -s /usr/bin/nodejs /usr/bin/node

---AVVIO---
Sul browser:
www.molly.dev




##Tool allineamento audio## (cartella audioalign)
Si tratta di un programma Java realizzato con Eclipse.
Da eclipse si può aprire il progetto con il percorso:

File->Open Projects From File System e selezionando la cartella "audioalign"

Nel sistema devono essere installati i programmi ffmpeg e ffprobe a livello
globale. La versione utilizzata di ffmpeg e' la 3.0.7
Si può esportare il file eseguibile jar tramite

File->Export->Java->Runnable jar File e spuntando la casella per includere le librerie

Un jar esportato è già presente nella cartella "audioalign/build"
Il funzionamento base del programma è:

java -jar audioalign.jar nomefileRef nomefileTar

che confronta i file nomefileRef e nomefileTar stampando in output lo scostamento
del secondo rispetto al primo.

Per il test al CSC è stato modificato specificatamente il codice affinché, date due cartelle
in unput, venghino cercate in queste le coppie di file che differiscono
per la sola estensione, wav o mov.

Ora questa modifica si può ottenere tramite l'opzione "--mode=folder"

ES:
java -jar audioalign.jar --mode=folder audiosfolder videosfolder

Al CSC il test era stato effettuato sulle cartelle:
/dati/csc/backup_NAS/backup/mac-audio/BERIO/AUDIO_ALE_VALE/  (file wav)
e
/dati/csc/backup_NAS/backup/mac-video/BERIO/video/Alessandro_Valentina/  (file mov)

Si erano prima create delle copie dei file, usando le opzioni -ss e -t di ffmpeg,
per avere i primi trenta secondi dei file.

Ora questo si può ottenere direttamente con le opzioni --startTime --stopTime, indicando in secondi il punto iniziale
e finale della porzione da analizzare
ES:
java -jar audioalign.jar --mode=folder audiosfolder videosfolder --startTime=0.0 --stopTime=30.0

Il pacchetto jar si trova nella cartella "bin" della webapp per lanciare eseguire la
sincronizzazione sul server

Note:
A casa non ho potuto riprovare queste modifiche sui file del CSC ma ho fatto
un test sulle cartelle "audioalign/testcases/testfolder/audio" e "audioalign/testcases/testfolder/video"
e mi pare funzionino correttamente.
I risultati del test al CSC sono scritti nel file "talign.txt" mentre il file "taling.csv"
viene scritto automaticamente quando si lancia il programma in modalità "folder"






## App Android ##(cartella android)

Si tratta di un progetto Android Studio.
Non ho un'altro PC dove poter installare Android Studio e Android SDK
ma credo che si possa importare senza grandi problemi, con qualche smanettamento.
La versione di SDK usata è la 24 come si può vedere dal file "android/app/build.gradle"

Per il corretto funzionamento è necessario che il file "string.xml" venga modificato
con il corretto dominio della piattaforma.