-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 28, 2017 at 04:04 PM
-- Server version: 10.0.27-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `molly`
--
CREATE DATABASE IF NOT EXISTS `molly` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `molly`;

-- --------------------------------------------------------

--
-- Table structure for table `awareness_context`
--

CREATE TABLE `awareness_context` (
  `id` int(11) NOT NULL,
  `weather_temperature` float DEFAULT NULL COMMENT 'celsius degrees',
  `weather_conditions` tinytext COMMENT 'array di interi che rappresentano le condizioni metereologiche',
  `weather_humidity` int(11) DEFAULT NULL,
  `location_latitude` float DEFAULT NULL,
  `location_longitude` float DEFAULT NULL,
  `location_altitude` float DEFAULT NULL,
  `location_accuracy` float DEFAULT NULL,
  `speed` float DEFAULT NULL,
  `headphone_plugged` tinyint(1) DEFAULT NULL,
  `activity_type` int(11) DEFAULT NULL,
  `activity_confidence` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `creator`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Classe CSC', 'The Centro di Sonologia Computazionale has been mainly a centre of promotion and cultural diffusion of music informatics since its foundation. Thanks to close collaboration among experts of various disciplines, it has been possible to create an interdisciplinary group, which has become an international reference in the field, and has come to be part of contemporary music history. Activities of CSC can be grouped into four main areas: scientific research, music research, production and performance of music works, teaching and dissemination.', '2017-03-19 20:41:14', '2017-03-20 23:23:02');

-- --------------------------------------------------------

--
-- Table structure for table `class_user_map`
--

CREATE TABLE `class_user_map` (
  `id` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `content_synchronization`
--

CREATE TABLE `content_synchronization` (
  `id` int(11) NOT NULL,
  `content_ids` text NOT NULL,
  `result` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `completed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dossier`
--

CREATE TABLE `dossier` (
  `id` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dossier_content_map`
--

CREATE TABLE `dossier_content_map` (
  `dossier` int(11) NOT NULL,
  `content` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `account_type` int(11) NOT NULL DEFAULT '0',
  `role` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `email`, `password_hash`, `account_type`, `role`, `created_at`, `updated_at`) VALUES
(1, 'mariorossi', 'mario', 'rossi', 'mario.rossi@studenti.unipd.it', '$2y$13$vo/JA4OFX.IWx4ccBi..yOOKQUt.rpQhwZM.ThUy8adkq7glnu1iW', 0, 'teacher', '2017-03-18 15:27:34', '2017-03-26 08:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_content`
--

CREATE TABLE `user_content` (
  `id` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `type` varchar(16) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(64) DEFAULT NULL,
  `awareness_context` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_content_comment`
--

CREATE TABLE `user_content_comment` (
  `id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `reference_comment` int(11) DEFAULT NULL,
  `author` int(11) NOT NULL,
  `text` tinytext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `awareness_context`
--
ALTER TABLE `awareness_context`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator` (`creator`);

--
-- Indexes for table `class_user_map`
--
ALTER TABLE `class_user_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class` (`class`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `content_synchronization`
--
ALTER TABLE `content_synchronization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dossier`
--
ALTER TABLE `dossier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator` (`creator`),
  ADD KEY `class` (`class`);

--
-- Indexes for table `dossier_content_map`
--
ALTER TABLE `dossier_content_map`
  ADD PRIMARY KEY (`dossier`,`content`),
  ADD KEY `content` (`content`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_content`
--
ALTER TABLE `user_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `awareness_context` (`awareness_context`),
  ADD KEY `creator` (`creator`);

--
-- Indexes for table `user_content_comment`
--
ALTER TABLE `user_content_comment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference_comment` (`reference_comment`),
  ADD KEY `content_id` (`content_id`),
  ADD KEY `author` (`author`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `awareness_context`
--
ALTER TABLE `awareness_context`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `class_user_map`
--
ALTER TABLE `class_user_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `content_synchronization`
--
ALTER TABLE `content_synchronization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dossier`
--
ALTER TABLE `dossier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_content`
--
ALTER TABLE `user_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `user_content_comment`
--
ALTER TABLE `user_content_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `user` (`id`);

--
-- Constraints for table `class_user_map`
--
ALTER TABLE `class_user_map`
  ADD CONSTRAINT `class_user_map_ibfk_1` FOREIGN KEY (`class`) REFERENCES `class` (`id`),
  ADD CONSTRAINT `class_user_map_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

--
-- Constraints for table `dossier`
--
ALTER TABLE `dossier`
  ADD CONSTRAINT `dossier_ibfk_1` FOREIGN KEY (`creator`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `dossier_ibfk_2` FOREIGN KEY (`class`) REFERENCES `class` (`id`);

--
-- Constraints for table `dossier_content_map`
--
ALTER TABLE `dossier_content_map`
  ADD CONSTRAINT `dossier_content_map_ibfk_1` FOREIGN KEY (`dossier`) REFERENCES `dossier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dossier_content_map_ibfk_2` FOREIGN KEY (`content`) REFERENCES `user_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_content`
--
ALTER TABLE `user_content`
  ADD CONSTRAINT `creator_to_user_fk` FOREIGN KEY (`creator`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_content_ibfk_1` FOREIGN KEY (`awareness_context`) REFERENCES `awareness_context` (`id`);

--
-- Constraints for table `user_content_comment`
--
ALTER TABLE `user_content_comment`
  ADD CONSTRAINT `user_content_comment_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `user_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_content_comment_ibfk_2` FOREIGN KEY (`reference_comment`) REFERENCES `user_content_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_content_comment_ibfk_3` FOREIGN KEY (`author`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


CREATE USER `molly`@`localhost` IDENTIFIED BY "yllom";
GRANT ALL PRIVILEGES ON molly.* TO molly@localhost;
FLUSH PRIVILEGES;